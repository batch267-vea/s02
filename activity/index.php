<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02-Activity</title>
</head>
<body>

	<h1>Divisible of Five</h1>
	<?php printDivisibleOfFive(); ?><br><br><br>


	<h1>Array Manipulation</h1>
	<p><?php array_push($students, "John Smith") ?></p>
	<p><?php var_dump($students) ?></p>
	<p><?php echo count($students) ?></p>

	<p><?php array_push($students, "Jane Smith") ?></p>
	<p><?php var_dump($students) ?></p>
	<p><?php echo count($students) ?></p>

	<p><?php array_shift($students) ?></p>
	<p><?php var_dump($students) ?></p>
	<p><?php echo count($students) ?></p>



</body>
</html>